package queue

import (
	"errors"
	"fmt"
	"time"
)

type Message struct {
	ID       int       `json:"id"`
	Name     string    `json:"name"`
	Action   string    `json:"action"`
	Value    int       `json:"value"`
	Time     time.Time `json:"time"`
	Status   string    `json:"status"`
	Runner   string    `json:"runner"`
	FinishAt time.Time `json:"finishat"`
}

type Queue struct {
	name     string    `json:"name"`
	locked   bool      `json:"locked"`
	filename string    `json:"filename"`
	messages []Message `json:"messages"`
}

func New(Name string) *Queue {
	fmt.Println("Create Queue")
	e := new(Queue)
	e.name = Name
	e.locked = false
	e.filename = "default.save"
	e.messages = []Message{}
	return e
}

func (m Message) ToStr() string {
	s := fmt.Sprintf("Id: %d, Name: %s, Action: %s, Value: %d ,Time: %s, Status: %s", m.ID, m.Name, m.Action, m.Time, m.Status)
	return s
}

func (m Message) GetAction() string {
	return m.Action
}

func (m Message) GetValue() int {
	return m.Value
}

func (m Message) GetID() int {
	return m.ID
}

func (m Message) GetName() string {
	return m.Name
}

func (m *Message) SetStatus(status string) {
	m.Status = status
}

func (m *Message) SetRunner(clientip string) {
	m.Runner = clientip
}

func (e Queue) Lock() {
	e.locked = true
	fmt.Printf("Queue %s is locked\n", e.name)
}

func (e Queue) Unlock() {
	e.locked = false
	fmt.Printf("Queue %s is unlock\n", e.name)
}

func (e Queue) ToStr() string {
	s := fmt.Sprintf("Name: %s, Lock %t, Messages: %d", e.name, e.locked, len(e.messages))
	return s
}

func (e Queue) DumpMessages() {
	for i, m := range e.messages {
		fmt.Println(i, m.ToStr())
	}
}

func (e *Queue) Push(name, action string, value int) *Message {
	fmt.Println("Push new message")
	m := NewMessage(len(e.messages), name, action, value)
	//fmt.Println("%+v", e.messages)
	e.messages = append(e.messages, *m)
	//fmt.Println("%+v", e.messages)

	return m
}

func (e *Queue) GetMessage() Message {

	e.messages[0].Status = "Get"
	r := e.messages[0]
	e.messages = e.messages[1:]
	return r
}

func (e *Queue) SetMessageStatus(index int, status string) *Message {
	e.messages[index].Status = status
	r := &e.messages[0]
	return r
}

func (e *Queue) SetMessageRunner(index int, clientip string) *Message {
	e.messages[index].Runner = clientip
	r := &e.messages[0]
	return r
}

func (e *Queue) SetMessageFinishAt(index int) *Message {
	e.messages[index].FinishAt = time.Now()
	r := &e.messages[0]
	return r
}

func RemoveIndex(s []Message, index int) []Message {
	return append(s[:index], s[index+1:]...)
}
func (e *Queue) DeleteMessage(index int) *Queue {
	e.messages = RemoveIndex(e.messages, index)
	r := e
	return r
}

func (e *Queue) NextMessage() (Message, error) {
	result := Message{}
	flag := false
	for _, mes := range e.messages {
		if mes.Status == "new" {
			fmt.Printf("Analyse de %+v", mes)
			result = mes
			flag = true
			break
		}
	}
	if flag {
		return result, nil
	} else {

		return result, errors.New("No messages")
	}
}

func NewMessage(id int, name, action string, value int) *Message {
	m := new(Message)
	m.ID = id
	m.Name = name
	m.Action = action
	m.Status = "new"
	m.Time = time.Now()
	m.Value = value
	return m
}

func (e Queue) Messages() []Message {
	return e.messages
}

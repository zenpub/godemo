package main

import (
	"encoding/gob"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"demo/server/queue"

	"github.com/gin-gonic/gin"
)

var e = queue.New("Tasks")

func postTask(c *gin.Context) {

	// Call BindJSON to bind the received JSON to
	// newAlbum.
	value, err := strconv.Atoi(c.PostForm("value"))
	if err != nil {
		value = 0
	}
	e.Push(c.PostForm("name"), c.PostForm("action"), value)

	c.Redirect(http.StatusMovedPermanently, "/tasks")

	//c.IndentedJSON(http.StatusCreated, newAlbum)
}

func setTaskStatus(c *gin.Context) {
	index, _ := strconv.Atoi(c.PostForm("id"))
	fmt.Printf("Status set to %s on task id %d\n", c.PostForm("status"), index)
	e.SetMessageStatus(index, c.PostForm("status"))
}

func setTaskFinishAt(c *gin.Context) {
	index, _ := strconv.Atoi(c.PostForm("id"))
	fmt.Printf("Status set to %s on task id %d\n", c.PostForm("status"), index)
	e.SetMessageFinishAt(index)
}

func delTask(c *gin.Context) {
	taskId, _ := strconv.Atoi(c.Param("id"))

	fmt.Printf("Delete task id %d\n", taskId)
	e.DeleteMessage(taskId)
}

func saveData() error {
	dataFile, err := os.Create("queue.gob")
	if err != nil {
		log.Fatal(err)
		return errors.New("Error on save data")
	}
	dataEncoder := gob.NewEncoder(dataFile)
	dataEncoder.Encode(e)
	dataFile.Close()
	return nil
}

func GetsaveData(c *gin.Context) {
	err := saveData()
	if err != nil {
		c.IndentedJSON(http.StatusOK, gin.H{"message": "Erreur"})

	} else {
		c.IndentedJSON(http.StatusOK, gin.H{"message": "album saved"})
	}
}

func loadData() error {
	dataFile, err := os.Open("queue.gob")

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	dataDecoder := gob.NewDecoder(dataFile)
	err = dataDecoder.Decode(&e)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	dataFile.Close()
	return nil
}
func getTasks(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, e.Messages())
}

func nextTask(c *gin.Context) {
	var result, err = e.NextMessage()
	e.SetMessageRunner(result.ID, c.ClientIP())
	fmt.Printf("Client: %s get task\n", c.ClientIP())
	if err != nil {
		c.IndentedJSON(http.StatusOK, nil)

	} else {
		c.IndentedJSON(http.StatusOK, result)

	}
}

func main() {

	log.SetPrefix("Web-service: ")
	log.SetFlags(0)
	log.Printf("Load date")
	fmt.Printf("%s\n", e.ToStr())
	e.Push("Load 01", "Load", 10)
	router := gin.Default()
	router.LoadHTMLGlob("templates/**/*.tmpl")
	router.Static("/assets", "./assets")
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "home/index.tmpl", gin.H{
			"title": "Main website",
		})
	})
	router.GET("/tasks", func(c *gin.Context) {
		c.HTML(http.StatusOK, "home/tasks.tmpl", gin.H{
			"title": "Tasks List", "tasks": e.Messages(),
		})
	})
	router.POST("/addtask", postTask)

	router.GET("/api/tasks", getTasks)
	router.GET("/api/nexttask", nextTask)
	router.POST("/api/task/status", setTaskStatus)
	router.POST("/api/task/finishtask", setTaskFinishAt)
	router.DELETE("/api/task/:task-id", delTask)

	router.Run(":8090")
}

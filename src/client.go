package main

import (
	"demo/server/queue"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

var timeSeconds int
var serverUrl string

func load(m queue.Message) {
	fmt.Println("Run Load")
	fmt.Printf("New Message !:%+v", m)

}

func RunCPULoad(timeSeconds int) {
	n := runtime.NumCPU()
	runtime.GOMAXPROCS(n)

	quit := make(chan bool)

	for i := 0; i < n; i++ {
		go func() {
			for {
				select {
				case <-quit:
					return
				default:
				}
			}
		}()
	}

	time.Sleep(time.Duration(timeSeconds) * time.Second)
	for i := 0; i < n; i++ {
		quit <- true
	}
}

func clientSetTaskStatus(id int, status string) {
	data := url.Values{
		"id":     {strconv.Itoa(id)},
		"status": {status},
	}

	resp, err := http.PostForm(serverUrl+"/api/task/status", data)
	if err != nil {
		log.Fatal(err)
	}
	var res map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&res)
	fmt.Println(res["form"])
}

func clientSetTaskFinishAt(id int) {
	data := url.Values{
		"id": {strconv.Itoa(id)},
	}

	resp, err := http.PostForm(serverUrl+"/api/task/finishtask", data)
	if err != nil {
		log.Fatal(err)
	}
	var res map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&res)
	fmt.Println(res["form"])
}

func main() {
	serverUrl = "http://" + os.Getenv("SERVER_SERVICE_HOST") + ":" + os.Getenv("SERVER_SERVICE_PORT")
	fmt.Printf("Connect to %s\n", serverUrl)
	for {
		fmt.Println("Try to get Message")
		response, err := http.Get(serverUrl + "/api/nexttask")

		if err != nil {
			fmt.Print(err.Error())
			os.Exit(1)
		}

		responseData, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Fatal(err)
		}
		data := new(queue.Message)
		myerr := json.Unmarshal(responseData, &data)
		if myerr != nil {
			fmt.Println("Error")
		} else {
			if data != nil {
				clientSetTaskStatus(data.GetID(), "running")
				if strings.ToLower(data.Action) == "stop" {
					clientSetTaskFinishAt(data.GetID())
					clientSetTaskStatus(data.GetID(), "terminated")
					os.Exit(0)
				} else if strings.ToLower(data.Action) == "load" {
					fmt.Printf("Start Task Load ID:%d during %d secondes\n", data.GetID(), data.GetValue())

					timeSeconds = data.GetValue()
					RunCPULoad(timeSeconds)
					fmt.Printf("End Task ID:%d\n", data.GetID())
					clientSetTaskFinishAt(data.GetID())
					clientSetTaskStatus(data.GetID(), "terminated")

				} else {
					fmt.Printf("Unkwon Action %s\n", data.GetAction())
				}
			} else {
				fmt.Println("No message.")
			}
		}
		fmt.Println("Wait")
		time.Sleep(8 * time.Second)
	}

}

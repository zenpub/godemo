# syntax=docker/dockerfile:1

FROM golang:1.18-alpine

WORKDIR /app

COPY src/go.mod ./
COPY src/go.sum ./
RUN go mod download

COPY src/server.go ./
COPY src/assets/ ./assets
COPY src/queue/ ./queue
COPY src/templates ./templates
RUN go build server.go 

EXPOSE 8090

CMD [ "/app/server" ]


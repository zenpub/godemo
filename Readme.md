# Build

## Build Server
```bash
docker build -t demo-server:v1 .
```
## Build Client
```bash
docker build -t demo-client:v1 -f Dockerfileclient  .
```

Client use two env variables, before start client set:

- SERVER_SERVICE_HOST: Server ip
- SERVER_SERVICE_PORT: Server Port

example:
```bash
  cd src 
  export SERVER_SERVICE_HOST="192.168.88.15"
  export SERVER_SERVICE_PORT="8090"
  go run client.go
```


# Export Image for k3s
```bash
cd images
docker save --output demo-server-v1.tar demo-server:v1
docker save --output demo-client-v1.tar demo-client:v1
```

# upload images:
```bash
scp images/*.tar dist:/dir...
```

# on k3s
## import images
```bash
sudo k3s ctr images import ./images/demo-server-v1.tar
sudo k3s ctr images import ./images/demo-client-v1.tar
```
## check
```bas
sudo k3s ctr images ls | grep demo
```
# Reset namespace
```bash
kubectl delete -f app.yaml
```
# Deploy app
```bash
kubectl apply -f app.yaml
kubectl apply -f client.yaml
```

# on k8s
```bash
docker tag demo-server:v1 yourregistry/godemo/server:v1
docker push yourregistry/godemo/server:v1

docker tag demo-client:v1 yourregistry/godemo/client:v1 
docker push yourregistry/godemo/client:v1
```

```bash
kubectl apply -f k8s/deployment-srv.yaml
kubectl apply -f k8s/deployment-client.yaml
```
